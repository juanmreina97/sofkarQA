package co.com.sofka.sofkar.userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class CrearNuevoVehiculo extends PageObject {
    public static final Target BOTON_AGREGAR_VEHICULO = Target
            .the("Botón agregar vehiculo")
            .located(By.xpath("//div[contains(text(), \"Agregar vehiculo\")]"));

    public static final Target FOTO_TRASERA = Target
            .the("Foto trasera")
            .located(By.id("foto-1"));

    public static final Target FOTO_DERECHA = Target
            .the("Foto derecha")
            .located(By.id("foto-2"));

    public static final Target FOTO_IZQUIERDA = Target
            .the("Foto izquierda")
            .located(By.id("foto-3"));

    public static final Target FOTO_MOTOR = Target
            .the("Foto motor")
            .located(By.id("foto-4"));

    public static final Target FOTO_INTERIOR = Target
            .the("Foto interior")
            .located(By.id("foto-5"));

    public static final Target FOTO_BAUL = Target
            .the("Foto baul")
            .located(By.id("foto-6"));

    public static final Target FOTO_LLANTAS = Target
            .the("Foto llantas")
            .located(By.id("foto-7"));

    public static final Target FOTO_FRONTAL = Target
            .the("Foto frontal")
            .located(By.id("foto-0"));

    public static final Target MARCA = Target
            .the("Marca")
            .located(By.id("marca"));

    public static final Target MODELO = Target
            .the("Modelo")
            .located(By.id("modelo"));

    public static final Target ANIO = Target
            .the("Año")
            .located(By.id("anio"));

    public static final Target COLOR = Target
            .the("Color")
            .located(By.id("color"));

    public static final Target KILOMETRAJE = Target
            .the("Kilometraje")
            .located(By.id("kilometros"));

    public static final Target CILINDRAJE = Target
            .the("Cilindraje")
            .located(By.id("cilindraje"));

    public static final Target TIPO_COMBUSTIBLE = Target
            .the("Tipo de combustible")
            .located(By.id("tipoCombustible"));

    public static final Target TIPO_CAJA = Target
            .the("Tipo de caja")
            .located(By.id("tipoCaja"));

    public static final Target TIPO_TRACCION = Target
            .the("Tipo de traccion")
            .located(By.id("tipoTraccion"));

    public static final Target DEPARTAMENTO = Target
            .the("Departamento")
            .located(By.id("departamento"));

    public static final Target MUNICIPIO = Target
            .the("Municipio")
            .located(By.id("municipios"));

    public static final Target FECHA_SOAT = Target
            .the("Fecha soat")
            .located(By.id("fechaVencimientoSOAT"));

    public static final Target VALOR = Target
            .the("Valor")
            .located(By.id("valor"));

    public static final Target EXTRA_CAMARA_REVERSA = Target
            .the("Camara reversa")
            .located(By.id("camaraReversa"));

    public static final Target EXTRA_POLARIZADO = Target
            .the("Polarizado")
            .located(By.id("polarizado"));

    public static final Target EXTRA_RETROVISORES_ELECTRICOS = Target
            .the("Retrovisores electricos")
            .located(By.id("retrovisoresElectricos"));

    public static final Target EXTRA_COJINERIA_CUERO = Target
            .the("Cojineria de cuero")
            .located(By.id("cojineriaCuero"));

    public static final Target EXTRA_VIDRIOS_ELECTRICOS = Target
            .the("Vidrios electricos")
            .located(By.id("vidriosElectricos"));

    public static final Target CONTACTO_VENDEDOR = Target
            .the("Contacto del vendedor")
            .located(By.id("contactoVendedor"));

    public static final Target BOTON_PUBLICAR = Target
            .the("Boton publicar")
            .located(By.xpath("//button[contains(text(), \"Publicar\")]"));

    public static final Target ASSERTION_CAMPO_OBLIGATORIO_COLOR = Target
            .the("Campo de color obligatorio")
            .located(By.xpath(""));

    public static final Target ASSERTION_ALERTA_CAMPOS_INCOMPLETOS = Target
            .the("Alerta campos incompletos")
            .located(By.xpath("//div[contains(text(), \"Información incompleta\")]"));

    public static final Target ASSERTION_VEHICULO_REGISTRADO = Target
            .the("Vehiculo registrado")
            .located(By.xpath("//div[contains(text(), \"Publicación exitosa\")]"));
}
