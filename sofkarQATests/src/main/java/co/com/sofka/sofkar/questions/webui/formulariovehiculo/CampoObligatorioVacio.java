package co.com.sofka.sofkar.questions.webui.formulariovehiculo;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static co.com.sofka.sofkar.userinterface.CrearNuevoVehiculo.ASSERTION_CAMPO_OBLIGATORIO_COLOR;

public class CampoObligatorioVacio implements Question {
    @Override
    public Boolean answeredBy(Actor actor) {
        return ASSERTION_CAMPO_OBLIGATORIO_COLOR.resolveFor(actor).isDisplayed();
    }

    public static CampoObligatorioVacio campoObligatorioVacio() {
        return new CampoObligatorioVacio();
    }
}
