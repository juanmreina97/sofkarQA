package co.com.sofka.sofkar.userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class Login extends PageObject {
    public static final Target LOGIN_EMAIL = Target
            .the("Email")
            .located(By.id("email"));

    public static final Target LOGIN_PASSWORD = Target
            .the("Password")
            .located(By.id("password"));

    public static final Target LOGIN_BUTTON = Target
            .the("Email")
            .located(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/form/button"));

    public static final Target REGISTER = Target
            .the("Register")
            .located(By.linkText("Registrate"));

    public static final Target ASSERTION_SIGN_OUT = Target
            .the("Boton cerrar sesion")
            .located(By.xpath("//*[name()=\"svg\" and @ data-testid=\"LogoutOutlinedIcon\"]"));

              //  .located(By.xpath("//button[contains(text(), 'Cerrar Sesion')]"));

    public static final Target ASSERTION_ERROR_EMAIL = Target
            .the("error_mail")
            .located(By.xpath("//div[contains(text(), 'El usuario no existe')]"));

    public static final Target ASSERTION_ERROR_CONTRASENA = Target
            .the("error_contrasena")
            .located(By.xpath("//div[contains(text(), 'La contraseña es incorrecta')]"));
}
