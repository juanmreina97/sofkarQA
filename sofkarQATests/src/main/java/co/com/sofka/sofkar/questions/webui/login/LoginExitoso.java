package co.com.sofka.sofkar.questions.webui.login;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static co.com.sofka.sofkar.userinterface.Login.ASSERTION_SIGN_OUT;

public class LoginExitoso implements Question {
    @Override
    public Boolean answeredBy(Actor actor) { return ASSERTION_SIGN_OUT.resolveFor(actor).isDisplayed();}

    public static LoginExitoso loginExitoso(){
        return new LoginExitoso();

    }

}
