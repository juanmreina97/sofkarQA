package co.com.sofka.sofkar.util;

import co.com.sofka.sofkar.models.LoginModel;
import co.com.sofka.sofkar.models.NewCarModel;
import co.com.sofka.sofkar.models.RegisterModel;
import com.github.javafaker.Faker;

import java.util.Locale;
import java.util.Random;

import static co.com.sofka.sofkar.util.Dictionary.*;

public class CreateModels {

    public static RegisterModel registerModelData(String language, String country, String emailDom) {
        Faker faker = Faker.instance(
                new Locale(language, country),
                new Random()
        );
        RegisterModel registerModel = new RegisterModel();
        registerModel.setPrimerNombre(faker.name().firstName());
        registerModel.setSegundoNombre(faker.name().firstName());
        registerModel.setPrimerApellido(faker.name().lastName());
        registerModel.setSegundoApellido(faker.name().lastName());
        registerModel.setDocumento(faker.idNumber().valid());
        registerModel.setCelular(String.valueOf(faker.number().numberBetween(300000000, 399999999))
                .concat(String.valueOf(faker.number().numberBetween(0, 9))));
        registerModel.setEmail(faker.name().username().concat(emailDom));

        registerModel.setTipoDocumento(TIPO_DOCUMENTO_DEFAULT);
        registerModel.setPassword(DEFAULT_CORRECT_PASSWORD);
        registerModel.setConfirmarPassword(DEFAULT_CORRECT_PASSWORD);
        registerModel.setShortPassword(DEFAULT_SHORT_PASSWORD);
        registerModel.setLowerCasePassword(DEFAULT_LOWER_CASE_PASSWORD);

        return registerModel;
    }

    public static LoginModel loginModelData(String email, String Password2) {
        LoginModel loginModel = new LoginModel();

        loginModel.setEmail(EMAIL_LOGIN);
        loginModel.setPassword2(PASS_LOGIN);

        loginModel.setEmail2(NOT_EMAIL_LOGIN);

        return loginModel;
    }

    public static NewCarModel newCarModelData(String language, String country) {
        Faker faker = Faker.instance(
                new Locale(language, country),
                new Random()
        );
        NewCarModel newCarModel = new NewCarModel();
        newCarModel.setAnio(String.valueOf(faker.number().numberBetween(2008, 2022)));
        newCarModel.setColor(faker.color().name());
        newCarModel.setKm(String.valueOf(faker.number().numberBetween(10000, 200000)).concat(" km"));
        newCarModel.setCilindraje(String.valueOf(faker.number().numberBetween(6, 60) * 100).concat(" cc"));
        //newCarModel.setDiaSoat(faker.number().numberBetween(1, 28));
        //newCarModel.setMesSoat(faker.number().numberBetween(1, 12));
        //newCarModel.setAnioSoat(faker.number().numberBetween(2020, 2023));
        newCarModel.setValor(String.valueOf(faker.number().numberBetween(50000000, 130000000)));
        newCarModel.setContactoVendedor(String
                .valueOf(
                        faker.number().numberBetween(300000000, 399999999))
                .concat(
                        String.valueOf(faker.number().numberBetween(0, 9))
                ));

        newCarModel.setFotoFrontal(DEFAULT_FOTO_FRONTAL_VEHICULO);
        newCarModel.setFotoTrasera(DEFAULT_FOTO_TRASERA_VEHICULO);
        newCarModel.setFotoIzquierda(DEFAULT_FOTO_IZQUIERDA_VEHICULO);
        newCarModel.setFotoDerecha(DEFAULT_FOTO_DERECHA_VEHICULO);
        newCarModel.setFotoBaul(DEFAULT_FOTO_BAUL_VEHICULO);
        newCarModel.setFotoInterior(DEFAULT_FOTO_INTERIOR_VEHICULO);
        newCarModel.setFotoLlantas(DEFAULT_FOTO_LLANTAS_VEHICULO);
        newCarModel.setFotoMotor(DEFAULT_FOTO_MOTOR_VEHICULO);

        newCarModel.setMarca(DEFAULT_MARCA_VEHICULO);
        newCarModel.setModelo(DEFAULT_MODELO_VEHICULO);
        newCarModel.setTipoCombustible(DEFAULT_TIPO_COMBUSTIBLE_VEHICULO);
        newCarModel.setTipoCaja(DEFAULT_TIPO_CAJA_VEHICULO);
        newCarModel.setTipoTraccion(DEFAULT_TIPO_TRACCION_VEHICULO);
        newCarModel.setDepartamento(DEFAULT_DEPARTAMENTO_VEHICULO);
        newCarModel.setMunicipio(DEFAULT_MUNICIPIO_VEHICULO);
        newCarModel.setFechaSoat("25/05/2022");

        return newCarModel;
    }
}
