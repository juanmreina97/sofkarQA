package co.com.sofka.sofkar.questions.webui.login;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static co.com.sofka.sofkar.userinterface.Login.ASSERTION_ERROR_CONTRASENA;


public class LoginPassError implements Question {
    @Override
    public Boolean answeredBy(Actor actor) { return ASSERTION_ERROR_CONTRASENA.resolveFor(actor).isDisplayed();}

    public static LoginPassError loginPassError(){
        return new LoginPassError();

    }

}
