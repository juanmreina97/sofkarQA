package co.com.sofka.sofkar.tasks.webui.registro;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Scroll;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static co.com.sofka.sofkar.userinterface.Register.*;
import static co.com.sofka.sofkar.util.EnumTimeOut.FIVE;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class RegistroVacio implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                WaitUntil.the(PRIMER_NOMBRE, isVisible()).forNoMoreThan(FIVE.getValue()).seconds(),

                Scroll.to(REGISTER_BUTTON),
                Click.on(REGISTER_BUTTON),

                WaitUntil.the(ASSERTION_INFORMACION_INCOMPLETA, isVisible()).forNoMoreThan(FIVE.getValue()).seconds()
        );
    }

    public static RegistroVacio registroVacio() {
        return new RegistroVacio();
    }
}
