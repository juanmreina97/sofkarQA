package co.com.sofka.sofkar.questions.webui.registro;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static co.com.sofka.sofkar.userinterface.Register.ASSERTION_CAMPO_OBLIGATORIO_NOMBRE;

public class CamposObligatoriosVacios implements Question {
    @Override
    public Boolean answeredBy(Actor actor) {
        return ASSERTION_CAMPO_OBLIGATORIO_NOMBRE.resolveFor(actor).isDisplayed();
    }

    public static CamposObligatoriosVacios camposObligatoriosVacios() {
        return new CamposObligatoriosVacios();
    }
}
