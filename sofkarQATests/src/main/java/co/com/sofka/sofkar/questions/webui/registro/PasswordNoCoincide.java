package co.com.sofka.sofkar.questions.webui.registro;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static co.com.sofka.sofkar.userinterface.Register.ASSERTION_PASSWORD_NO_COINCIDE;

public class PasswordNoCoincide implements Question {
    @Override
    public Boolean answeredBy(Actor actor) {
        return ASSERTION_PASSWORD_NO_COINCIDE.resolveFor(actor).isDisplayed();
    }

    public static PasswordNoCoincide passwordNoCoincide() {
        return new PasswordNoCoincide();
    }
}
