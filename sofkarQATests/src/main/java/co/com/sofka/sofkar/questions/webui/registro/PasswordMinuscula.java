package co.com.sofka.sofkar.questions.webui.registro;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static co.com.sofka.sofkar.userinterface.Register.ASSERTION_MAYUSCULAS_MINUSCULAS_SIMBOLO_NUMERO_PASSWORD;

public class PasswordMinuscula implements Question {
    @Override
    public Boolean answeredBy(Actor actor) {
        return ASSERTION_MAYUSCULAS_MINUSCULAS_SIMBOLO_NUMERO_PASSWORD.resolveFor(actor).isDisplayed();
    }

    public static PasswordMinuscula passwordMinuscula() {
        return new PasswordMinuscula();
    }
}
