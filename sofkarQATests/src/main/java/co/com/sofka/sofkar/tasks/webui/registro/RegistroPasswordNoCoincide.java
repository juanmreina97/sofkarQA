package co.com.sofka.sofkar.tasks.webui.registro;

import co.com.sofka.sofkar.models.RegisterModel;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Scroll;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static co.com.sofka.sofkar.userinterface.Register.*;
import static co.com.sofka.sofkar.userinterface.Register.CONFIRMAR_PASSWORD;
import static co.com.sofka.sofkar.util.EnumTimeOut.FIVE;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class RegistroPasswordNoCoincide implements Task {
    private RegisterModel registerModel;

    public RegistroPasswordNoCoincide usingModel(RegisterModel registerModel) {
        this.registerModel = registerModel;
        return this;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                WaitUntil.the(PRIMER_NOMBRE, isVisible()).forNoMoreThan(FIVE.getValue()).seconds(),

                Scroll.to(REGISTER_PASSWORD),
                Enter.theValue(registerModel.getPassword()).into(REGISTER_PASSWORD),

                Scroll.to(CONFIRMAR_PASSWORD),
                Enter.theValue(registerModel.getLowerCasePassword()).into(CONFIRMAR_PASSWORD),

                Scroll.to(REGISTER_PASSWORD),
                Click.on(REGISTER_PASSWORD)
        );
    }

    public static RegistroPasswordNoCoincide registroPasswordNoCoincide() {
        return new RegistroPasswordNoCoincide();
    }
}
