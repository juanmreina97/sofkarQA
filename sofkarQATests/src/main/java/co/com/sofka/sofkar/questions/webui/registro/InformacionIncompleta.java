package co.com.sofka.sofkar.questions.webui.registro;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static co.com.sofka.sofkar.userinterface.Register.ASSERTION_INFORMACION_INCOMPLETA;

public class InformacionIncompleta implements Question {
    @Override
    public Boolean answeredBy(Actor actor) {
        return ASSERTION_INFORMACION_INCOMPLETA.resolveFor(actor).isDisplayed();
    }

    public static InformacionIncompleta informacionIncompleta() {
        return new InformacionIncompleta();
    }
}
