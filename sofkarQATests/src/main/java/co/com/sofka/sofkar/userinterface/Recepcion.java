package co.com.sofka.sofkar.userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class Recepcion extends PageObject {
    public static final Target BOTON_COMPRAR = Target
            .the("Boton comprar vehiculo")
            .located(By.xpath("//*[@id=\"root\"]/div/div/div/div/div[1]/div[1]/button"));

    public static final Target BOTON_VENDER = Target
            .the("Boton vender vehiculo")
            .located(By.xpath("//*[@id=\"root\"]/div/div/div/div/div[1]/div[2]/button"));

    public static final Target BOTON_CERRAR_SESION = Target
            .the("Boton cerrar sesion")
            .located(By.xpath("//*[name()=\"svg\" and @ data-testid=\"LogoutOutlinedIcon\"]"));
}
