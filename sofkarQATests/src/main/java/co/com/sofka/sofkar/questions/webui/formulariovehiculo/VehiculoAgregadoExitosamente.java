package co.com.sofka.sofkar.questions.webui.formulariovehiculo;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static co.com.sofka.sofkar.userinterface.CrearNuevoVehiculo.ASSERTION_VEHICULO_REGISTRADO;

public class VehiculoAgregadoExitosamente implements Question {
    @Override
    public Boolean answeredBy(Actor actor) {
        return ASSERTION_VEHICULO_REGISTRADO.resolveFor(actor).isDisplayed();
    }

    public static VehiculoAgregadoExitosamente vehiculoAgregadoExitosamente() {
        return new VehiculoAgregadoExitosamente();
    }
}
