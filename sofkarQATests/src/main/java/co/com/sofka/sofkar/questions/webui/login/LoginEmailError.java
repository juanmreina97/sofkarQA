package co.com.sofka.sofkar.questions.webui.login;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static co.com.sofka.sofkar.userinterface.Login.ASSERTION_ERROR_EMAIL;


public class LoginEmailError implements Question {
    @Override
    public Boolean answeredBy(Actor actor) { return ASSERTION_ERROR_EMAIL.resolveFor(actor).isDisplayed();}

    public static LoginEmailError loginEmailError(){
        return new LoginEmailError();

    }

}
