package co.com.sofka.sofkar.util;

import java.nio.file.Path;
import java.nio.file.Paths;

public class Dictionary {

    public static final String TIPO_DOCUMENTO_DEFAULT = "Cedula de ciudadania";
    public static final String DEFAULT_CORRECT_PASSWORD = "Test123*";
    public static final String DEFAULT_SHORT_PASSWORD = "Test";
    public static final String DEFAULT_LOWER_CASE_PASSWORD = "testtest";

    public static final String DEFAULT_MARCA_VEHICULO = "Renault";
    public static final String DEFAULT_MODELO_VEHICULO = "Koleos";
    public static final String DEFAULT_TIPO_COMBUSTIBLE_VEHICULO = "Gasolina";
    public static final String DEFAULT_TIPO_CAJA_VEHICULO = "Automatico";
    public static final String DEFAULT_TIPO_TRACCION_VEHICULO = "4X4";
    public static final String DEFAULT_DEPARTAMENTO_VEHICULO = "Antioquia";
    public static final String DEFAULT_MUNICIPIO_VEHICULO = "Medellín";
    public static final Path DEFAULT_FOTO_FRONTAL_VEHICULO = Paths.get(System.getProperty("user.dir") + "\\src\\main\\resources\\files\\frontal.png");
    public static final Path DEFAULT_FOTO_TRASERA_VEHICULO = Paths.get(System.getProperty("user.dir") + "\\src\\main\\resources\\files\\trasera.png");
    public static final Path DEFAULT_FOTO_IZQUIERDA_VEHICULO = Paths.get(System.getProperty("user.dir") + "\\src\\main\\resources\\files\\izquierda.png");
    public static final Path DEFAULT_FOTO_DERECHA_VEHICULO = Paths.get(System.getProperty("user.dir") + "\\src\\main\\resources\\files\\derecha.png");
    public static final Path DEFAULT_FOTO_BAUL_VEHICULO = Paths.get(System.getProperty("user.dir") + "\\src\\main\\resources\\files\\baul.png");
    public static final Path DEFAULT_FOTO_INTERIOR_VEHICULO = Paths.get(System.getProperty("user.dir") + "\\src\\main\\resources\\files\\interior.png");
    public static final Path DEFAULT_FOTO_LLANTAS_VEHICULO = Paths.get(System.getProperty("user.dir") + "\\src\\main\\resources\\files\\llantas.png");
    public static final Path DEFAULT_FOTO_MOTOR_VEHICULO = Paths.get(System.getProperty("user.dir") + "\\src\\main\\resources\\files\\motor.png");


    public static final String EMPTY_STRING = "";
    public static final String SPACE_STRING = " ";
    public static final String DOLLAR_SIGN_STRING = "$";
    public static final String QUESTION1_SIGN_STRING = "¿";
    public static final String QUESTION2_SIGN_STRING = "?";

    public static final String SPANISH_CODE_LANGUAGE = "es";
    public static final String COUNTRY_CODE = "MX";
    public static final String EMAIL_DOMAIN = "@sofkamail.com";

    public static final String EMAIL_LOGIN = "mejiamejiad@gmail.com";
    public static final String PASS_LOGIN = "David1991*";

    public static final String NOT_EMAIL_LOGIN = "noexiste@gmail.com";




}
