package co.com.sofka.sofkar.questions.webui.formulariovehiculo;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static co.com.sofka.sofkar.userinterface.CrearNuevoVehiculo.ASSERTION_ALERTA_CAMPOS_INCOMPLETOS;

public class CamposIncompletos implements Question {
    @Override
    public Boolean answeredBy(Actor actor) {
        return ASSERTION_ALERTA_CAMPOS_INCOMPLETOS.resolveFor(actor).isDisplayed();
    }

    public static CamposIncompletos camposIncompletos() {
        return new CamposIncompletos();
    }
}
