package co.com.sofka.sofkar.tasks.webui.formulariovehiculo;

import co.com.sofka.sofkar.models.NewCarModel;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.*;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static co.com.sofka.sofkar.userinterface.CrearNuevoVehiculo.*;
import static co.com.sofka.sofkar.util.Dictionary.*;
import static co.com.sofka.sofkar.util.EnumTimeOut.FIVE;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isClickable;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class FormularioCompletoCorrecto implements Task {
    private NewCarModel newCarModel;

    public FormularioCompletoCorrecto usingModel(NewCarModel newCarModel) {
        this.newCarModel = newCarModel;
        return this;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                WaitUntil.the(MARCA, isVisible()).forNoMoreThan(FIVE.getValue()).seconds(),

                Upload.theFile(DEFAULT_FOTO_FRONTAL_VEHICULO).to(FOTO_FRONTAL),
                Upload.theFile(DEFAULT_FOTO_TRASERA_VEHICULO).to(FOTO_TRASERA),
                Upload.theFile(DEFAULT_FOTO_DERECHA_VEHICULO).to(FOTO_DERECHA),
                Upload.theFile(DEFAULT_FOTO_IZQUIERDA_VEHICULO).to(FOTO_IZQUIERDA),
                Upload.theFile(DEFAULT_FOTO_MOTOR_VEHICULO).to(FOTO_MOTOR),
                Upload.theFile(DEFAULT_FOTO_INTERIOR_VEHICULO).to(FOTO_INTERIOR),
                Upload.theFile(DEFAULT_FOTO_BAUL_VEHICULO).to(FOTO_BAUL),
                Upload.theFile(DEFAULT_FOTO_LLANTAS_VEHICULO).to(FOTO_LLANTAS),

                Scroll.to(MARCA),
                SelectFromOptions.byVisibleText(newCarModel.getMarca()).from(MARCA),

                Scroll.to(MODELO),
                SelectFromOptions.byVisibleText(newCarModel.getModelo()).from(MODELO),

                Scroll.to(ANIO),
                Enter.theValue(newCarModel.getAnio()).into(ANIO),

                Scroll.to(COLOR),
                Enter.theValue(newCarModel.getColor()).into(COLOR),

                Scroll.to(KILOMETRAJE),
                Enter.theValue(newCarModel.getKm()).into(KILOMETRAJE),

                Scroll.to(CILINDRAJE),
                Enter.theValue(newCarModel.getCilindraje()).into(CILINDRAJE),

                Scroll.to(TIPO_COMBUSTIBLE),
                SelectFromOptions.byVisibleText(newCarModel.getTipoCombustible()).from(TIPO_COMBUSTIBLE),

                Scroll.to(TIPO_CAJA),
                SelectFromOptions.byVisibleText(newCarModel.getTipoCaja()).from(TIPO_CAJA),

                Scroll.to(TIPO_TRACCION),
                SelectFromOptions.byVisibleText(newCarModel.getTipoTraccion()).from(TIPO_TRACCION),

                Scroll.to(DEPARTAMENTO),
                SelectFromOptions.byVisibleText(newCarModel.getDepartamento()).from(DEPARTAMENTO),

                Scroll.to(MUNICIPIO),
                SelectFromOptions.byVisibleText(newCarModel.getMunicipio()).from(MUNICIPIO),

                Scroll.to(FECHA_SOAT),
                Click.on(FECHA_SOAT),
                Enter.theValue(newCarModel.getFechaSoat()).into(FECHA_SOAT),

                Scroll.to(EXTRA_CAMARA_REVERSA),
                Click.on(EXTRA_CAMARA_REVERSA),

                Scroll.to(EXTRA_POLARIZADO),
                Click.on(EXTRA_POLARIZADO),

                Scroll.to(EXTRA_RETROVISORES_ELECTRICOS),
                Click.on(EXTRA_RETROVISORES_ELECTRICOS),

                Scroll.to(EXTRA_VIDRIOS_ELECTRICOS),
                Click.on(EXTRA_VIDRIOS_ELECTRICOS),

                Scroll.to(EXTRA_COJINERIA_CUERO),
                Click.on(EXTRA_COJINERIA_CUERO),

                Scroll.to(CONTACTO_VENDEDOR),
                Enter.theValue(newCarModel.getContactoVendedor()).into(CONTACTO_VENDEDOR),

                Scroll.to(VALOR),
                Enter.theValue(newCarModel.getValor()).into(VALOR),

                Scroll.to(BOTON_PUBLICAR),
                Click.on(BOTON_PUBLICAR),

                WaitUntil.the(ASSERTION_VEHICULO_REGISTRADO, isVisible()).forNoMoreThan(FIVE.getValue()).seconds()
        );
    }

    public static FormularioCompletoCorrecto formularioCompletoCorrecto() {
        return new FormularioCompletoCorrecto();
    }
}
