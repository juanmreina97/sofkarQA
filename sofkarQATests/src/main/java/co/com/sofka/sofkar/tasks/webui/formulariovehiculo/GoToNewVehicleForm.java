package co.com.sofka.sofkar.tasks.webui.formulariovehiculo;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Scroll;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static co.com.sofka.sofkar.userinterface.CrearNuevoVehiculo.BOTON_AGREGAR_VEHICULO;
import static co.com.sofka.sofkar.userinterface.Recepcion.BOTON_VENDER;
import static co.com.sofka.sofkar.util.EnumTimeOut.FIVE;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class GoToNewVehicleForm implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                WaitUntil.the(BOTON_VENDER, isVisible()).forNoMoreThan(FIVE.getValue()).seconds(),

                Scroll.to(BOTON_VENDER),
                Click.on(BOTON_VENDER),

                WaitUntil.the(BOTON_AGREGAR_VEHICULO, isVisible()).forNoMoreThan(FIVE.getValue()).seconds(),
                Scroll.to(BOTON_AGREGAR_VEHICULO),
                Click.on(BOTON_AGREGAR_VEHICULO)
        );
    }

    public static GoToNewVehicleForm goToNewVehicleForm() {
        return new GoToNewVehicleForm();
    }
}
