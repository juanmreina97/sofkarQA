package co.com.sofka.sofkar.models;

import java.nio.file.Path;

public class NewCarModel {

    private Path fotoFrontal;
    private Path fotoTrasera;
    private Path fotoDerecha;
    private Path fotoIzquierda;
    private Path fotoMotor;
    private Path fotoInterior;
    private Path fotoBaul;
    private Path fotoLlantas;

    private String marca;
    private String modelo;
    private String anio;
    private String color;
    private String km;
    private String cilindraje;
    private String tipoCombustible;
    private String tipoCaja;
    private String tipoTraccion;
    private String departamento;
    private String municipio;
    private String fechaSoat;
    private int diaSoat;
    private int mesSoat;
    private int anioSoat;
    private String contactoVendedor;
    private String valor;

    public String getFechaSoat() {
        return fechaSoat;
    }

    public void setFechaSoat(String fechaSoat) {
        this.fechaSoat = fechaSoat;
    }

    public String getContactoVendedor() {
        return contactoVendedor;
    }

    public void setContactoVendedor(String contactoVendedor) {
        this.contactoVendedor = contactoVendedor;
    }

    public Path getFotoFrontal() {
        return fotoFrontal;
    }

    public void setFotoFrontal(Path fotoFrontal) {
        this.fotoFrontal = fotoFrontal;
    }

    public Path getFotoTrasera() {
        return fotoTrasera;
    }

    public void setFotoTrasera(Path fotoTrasera) {
        this.fotoTrasera = fotoTrasera;
    }

    public Path getFotoDerecha() {
        return fotoDerecha;
    }

    public void setFotoDerecha(Path fotoDerecha) {
        this.fotoDerecha = fotoDerecha;
    }

    public Path getFotoIzquierda() {
        return fotoIzquierda;
    }

    public void setFotoIzquierda(Path fotoIzquierda) {
        this.fotoIzquierda = fotoIzquierda;
    }

    public Path getFotoMotor() {
        return fotoMotor;
    }

    public void setFotoMotor(Path fotoMotor) {
        this.fotoMotor = fotoMotor;
    }

    public Path getFotoInterior() {
        return fotoInterior;
    }

    public void setFotoInterior(Path fotoInterior) {
        this.fotoInterior = fotoInterior;
    }

    public Path getFotoBaul() {
        return fotoBaul;
    }

    public void setFotoBaul(Path fotoBaul) {
        this.fotoBaul = fotoBaul;
    }

    public Path getFotoLlantas() {
        return fotoLlantas;
    }

    public void setFotoLlantas(Path fotoLlantas) {
        this.fotoLlantas = fotoLlantas;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getAnio() {
        return anio;
    }

    public void setAnio(String anio) {
        this.anio = anio;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getKm() {
        return km;
    }

    public void setKm(String km) {
        this.km = km;
    }

    public String getCilindraje() {
        return cilindraje;
    }

    public void setCilindraje(String cilindraje) {
        this.cilindraje = cilindraje;
    }

    public String getTipoCombustible() {
        return tipoCombustible;
    }

    public void setTipoCombustible(String tipoCombustible) {
        this.tipoCombustible = tipoCombustible;
    }

    public String getTipoCaja() {
        return tipoCaja;
    }

    public void setTipoCaja(String tipoCaja) {
        this.tipoCaja = tipoCaja;
    }

    public String getTipoTraccion() {
        return tipoTraccion;
    }

    public void setTipoTraccion(String tipoTraccion) {
        this.tipoTraccion = tipoTraccion;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public int getDiaSoat() {
        return diaSoat;
    }

    public void setDiaSoat(int diaSoat) {
        this.diaSoat = diaSoat;
    }

    public int getMesSoat() {
        return mesSoat;
    }

    public void setMesSoat(int mesSoat) {
        this.mesSoat = mesSoat;
    }

    public int getAnioSoat() {
        return anioSoat;
    }

    public void setAnioSoat(int anioSoat) {
        this.anioSoat = anioSoat;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
}
