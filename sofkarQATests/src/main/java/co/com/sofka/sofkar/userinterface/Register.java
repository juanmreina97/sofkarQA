package co.com.sofka.sofkar.userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class Register extends PageObject {
    public static final Target PRIMER_NOMBRE = Target
            .the("Primer nombre")
            .located(By.id("nombre"));

    public static final Target SEGUNDO_NOMBRE = Target
            .the("Segundo nombre")
            .located(By.id("segundoNombre"));

    public static final Target PRIMER_APELLIDO = Target
            .the("Primer apellido")
            .located(By.id("apellido"));

    public static final Target SEGUNDO_APELLIDO = Target
            .the("Segundo apellido")
            .located(By.id("segundoApellido"));

    public static final Target TIPO_DOCUMENTO = Target
            .the("Tipo documento")
            .located(By.id("tipoDocumento"));

    public static final Target DOCUMENTO = Target
            .the("Documento")
            .located(By.id("documento"));

    public static final Target CELULAR = Target
            .the("Celular")
            .located(By.id("celular"));

    public static final Target REGISTER_EMAIL = Target
            .the("Email")
            .located(By.id("email"));

    public static final Target REGISTER_PASSWORD = Target
            .the("Password")
            .located(By.id("password"));

    public static final Target CONFIRMAR_PASSWORD = Target
            .the("Confirmar password")
            .located(By.id("confirmarPassword"));

    public static final Target REGISTER_BUTTON = Target
            .the("Botón de registrar")
            .located(By.xpath("//*[@id=\"root\"]/div/div/div/div/form/div[5]/button"));

    public static final Target ASSERTION_CAMPO_OBLIGATORIO_NOMBRE = Target
            .the("Campo de nombre obligatorio")
            .located(By.xpath("//p[contains(text(), \"obligatorio\")]"));

    public static final Target ASSERTION_8_CARACTERES_PASSWORD = Target
            .the("Password de 8 caracteres")
            .located(By.xpath("//p[contains(text(), \"8 caracteres\")]"));

    public static final Target ASSERTION_MAYUSCULAS_MINUSCULAS_SIMBOLO_NUMERO_PASSWORD = Target
            .the("Password con mayusculas, minusculas, numeros y simbolos")
            .located(By.xpath("//p[contains(text(), \"caracter especial\")]"));

    public static final Target ASSERTION_PASSWORD_NO_COINCIDE = Target
            .the("Password no coincide")
            .located(By.xpath("//p[contains(text(), \"no coinciden\")]"));

    public static final Target ASSERTION_FORMATO_EMAIL_INVALIDO = Target
            .the("Formato de email invalido")
            .located(By.xpath("//p[contains(text(), \"formato de email\")]"));

    public static final Target ASSERTION_EMAIL_YA_USADO = Target
            .the("Email ya esta en uso")
            .located(By.xpath("//div[contains(text(), \"email ya esta en uso\")]"));

    public static final Target ASSERTION_USUARIO_REGISTRADO = Target
            .the("Usuario registrado")
            .located(By.xpath("//div[contains(text(), \"Usuario registrado\")]"));

    public static final Target ASSERTION_INFORMACION_INCOMPLETA = Target
            .the("Informacion de registro incompleta")
            .located(By.xpath("//div[contains(text(), \"Información incompleta\")]"));
}
