package co.com.sofka.sofkar.tasks.webui.registro;

import co.com.sofka.sofkar.models.RegisterModel;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Scroll;
import net.serenitybdd.screenplay.actions.SelectFromOptions;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static co.com.sofka.sofkar.userinterface.Register.*;
import static co.com.sofka.sofkar.util.EnumTimeOut.FIVE;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class RegistroCorrectoCamposObligatorios implements Task {
    private RegisterModel registerModel;

    public RegistroCorrectoCamposObligatorios usingModel(RegisterModel registerModel) {
        this.registerModel = registerModel;
        return this;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                WaitUntil.the(PRIMER_NOMBRE, isVisible()).forNoMoreThan(FIVE.getValue()).seconds(),

                Scroll.to(PRIMER_NOMBRE),
                Enter.theValue(registerModel.getPrimerNombre()).into(PRIMER_NOMBRE),

                Scroll.to(PRIMER_APELLIDO),
                Enter.theValue(registerModel.getPrimerApellido()).into(PRIMER_APELLIDO),

                Scroll.to(TIPO_DOCUMENTO),
                SelectFromOptions.byVisibleText(registerModel.getTipoDocumento()).from(TIPO_DOCUMENTO),

                Scroll.to(DOCUMENTO),
                Enter.theValue(registerModel.getDocumento()).into(DOCUMENTO),

                Scroll.to(CELULAR),
                Enter.theValue(registerModel.getCelular()).into(CELULAR),

                Scroll.to(REGISTER_EMAIL),
                Enter.theValue(registerModel.getEmail()).into(REGISTER_EMAIL),

                Scroll.to(REGISTER_PASSWORD),
                Enter.theValue(registerModel.getPassword()).into(REGISTER_PASSWORD),

                Scroll.to(CONFIRMAR_PASSWORD),
                Enter.theValue(registerModel.getPassword()).into(CONFIRMAR_PASSWORD),

                Scroll.to(REGISTER_BUTTON),
                Click.on(REGISTER_BUTTON),

                WaitUntil.the(ASSERTION_USUARIO_REGISTRADO, isVisible()).forNoMoreThan(FIVE.getValue()).seconds()
                );
    }

    public static RegistroCorrectoCamposObligatorios registroCorrectoCamposObligatorios() {
        return new RegistroCorrectoCamposObligatorios();
    }
}
