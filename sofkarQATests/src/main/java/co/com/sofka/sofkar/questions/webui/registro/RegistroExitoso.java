package co.com.sofka.sofkar.questions.webui.registro;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static co.com.sofka.sofkar.userinterface.Register.ASSERTION_USUARIO_REGISTRADO;

public class RegistroExitoso implements Question {
    @Override
    public Boolean answeredBy(Actor actor) {
        return ASSERTION_USUARIO_REGISTRADO.resolveFor(actor).isDisplayed();
    }

    public static RegistroExitoso registroExitoso() {
        return new RegistroExitoso();
    }
}
