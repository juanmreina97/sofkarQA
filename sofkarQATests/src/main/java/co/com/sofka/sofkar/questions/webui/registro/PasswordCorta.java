package co.com.sofka.sofkar.questions.webui.registro;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static co.com.sofka.sofkar.userinterface.Register.ASSERTION_8_CARACTERES_PASSWORD;

public class PasswordCorta implements Question {
    @Override
    public Boolean answeredBy(Actor actor) {
        return ASSERTION_8_CARACTERES_PASSWORD.resolveFor(actor).isDisplayed();
    }

    public static PasswordCorta passwordCorta() {
        return new PasswordCorta();
    }
}
