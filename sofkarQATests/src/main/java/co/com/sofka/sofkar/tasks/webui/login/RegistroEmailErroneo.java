package co.com.sofka.sofkar.tasks.webui.login;

import co.com.sofka.sofkar.models.LoginModel;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Scroll;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static co.com.sofka.sofkar.userinterface.Login.*;
import static co.com.sofka.sofkar.util.EnumTimeOut.FIVE;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class RegistroEmailErroneo implements Task {

    private LoginModel loginModel;
    public RegistroEmailErroneo usingModel(LoginModel loginModel) {
        this.loginModel = loginModel;
        return this;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(

                WaitUntil.the(LOGIN_EMAIL, isVisible()).forNoMoreThan(FIVE.getValue()).seconds(),

                Scroll.to(LOGIN_EMAIL),
                Enter.theValue(loginModel.getEmail2()).into(LOGIN_EMAIL),

                Scroll.to(LOGIN_PASSWORD),
                Enter.theValue(loginModel.getPassword2()).into(LOGIN_PASSWORD),

                Scroll.to(LOGIN_BUTTON),
                Click.on(LOGIN_BUTTON),

                WaitUntil.the(ASSERTION_ERROR_EMAIL, isVisible()).forNoMoreThan(FIVE.getValue()).seconds()

        );

    }






    public static RegistroEmailErroneo registroEmailErroneo() {
        return new RegistroEmailErroneo();
    }
}
