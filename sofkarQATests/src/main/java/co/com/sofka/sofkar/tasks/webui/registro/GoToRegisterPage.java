package co.com.sofka.sofkar.tasks.webui.registro;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Scroll;

import static co.com.sofka.sofkar.userinterface.Login.REGISTER;

public class GoToRegisterPage implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Scroll.to(REGISTER),
                Click.on(REGISTER)
        );
    }

    public static GoToRegisterPage goToRegisterPage() {
        return new GoToRegisterPage();
    }
}
