package co.com.sofka.sofkar.tasks.webui.formulariovehiculo;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Scroll;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static co.com.sofka.sofkar.userinterface.CrearNuevoVehiculo.*;
import static co.com.sofka.sofkar.util.EnumTimeOut.FIVE;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class FormularioVacio implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                WaitUntil.the(MARCA, isVisible()).forNoMoreThan(FIVE.getValue()).seconds(),

                Scroll.to(BOTON_PUBLICAR),
                Click.on(BOTON_PUBLICAR),

                WaitUntil.the(ASSERTION_ALERTA_CAMPOS_INCOMPLETOS, isVisible()).forNoMoreThan(FIVE.getValue()).seconds()
        );
    }

    public static FormularioVacio formularioVacio() {
        return new FormularioVacio();
    }
}
