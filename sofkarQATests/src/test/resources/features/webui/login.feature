# language: es
Característica: login en la plataforma
  como usuario registrado
  quiero poder ingresar en la plataforma
  para poder comprar o vender un vehiculo

  Antecedentes:
    Dado que el usuario se encuentra en la pagina de ingreso

  Escenario: Ingreso exitoso
    Cuando el usuario llena correctamente las credenciales
    Entonces la plataforma mostrara la opcion de cerrar sesion

  Escenario: Ingreso de correo no registrado
    Cuando el usuario ingresa un correo no registrado
    Entonces el sistema mostrara un mensaje de error

  Escenario: Ingreso con contraseña incorrecta
    Cuando el usuario ingresa un password incorrecto
    Entonces el sistema lanzara mensaje de error