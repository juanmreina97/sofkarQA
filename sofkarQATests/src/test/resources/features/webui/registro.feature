# language: es
  Característica: Registro en la plataforma
  yo como usuario
  quiero poder registrarme en la plataforma
  para poder comprar o vender un vehiculo

    Antecedentes:
      Dado que el usuario se encuentra en la pagina de registro

    Escenario: Registro exitoso
      Cuando el usuario llena correctamente los campos obligatorios y confirma
      Entonces la plataforma mostrara un mensaje de registro exitoso

    Escenario: Registro com password menor a 8 caracteres
      Cuando el usuario ingresa un password con menos de ocho digitos
      Entonces el sistema mostrara un aviso de error por longitud de password

    Escenario: Registro con un password en minusculas
      Cuando el usuario ingresa un password de ocho digitos completamente en minuscula
      Entonces el sistema mostrara un aviso de error por falta de combinacion de caracteres

    Escenario: Registro con verificacion de password incorrecta
      Cuando los password ingresados por el usuario no coinciden
      Entonces el sistema mostrara un aviso por password diferentes

    Escenario: Registro con los campos obligatorios vacios
      Cuando el usuario se registra con campos obligatorios vacios
      Entonces el sistema mostrara un error de campo obligatorio vacio