# language: es

Característica: Agregar un vehiculo a la venta
  yo como usuario vendedor
  quiero poder agregar un vehículo a mi cuenta
  para poder tenerlo a la venta en la página

  Antecedentes:
    Dado que el usuario inicia sesion y navega al formulario de agregar vehiculo

  Escenario: Agregar un vehiculo exitosamente
    Cuando el usuario llena todos los campos correctamente y envia el formulario
    Entonces el sistema mostrara un aviso de vehiculo registrado

  Escenario: Enviar un formulario vacio
    Cuando el usuario envia el formulario vacio
    Entonces el sistema mostrara un aviso de campos incompletos