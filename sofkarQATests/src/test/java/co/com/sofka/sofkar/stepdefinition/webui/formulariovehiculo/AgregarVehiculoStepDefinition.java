package co.com.sofka.sofkar.stepdefinition.webui.formulariovehiculo;

import co.com.sofka.sofkar.models.LoginModel;
import co.com.sofka.sofkar.models.NewCarModel;
import co.com.sofka.sofkar.stepdefinition.setup.SetUp;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import org.apache.log4j.Logger;

import static co.com.sofka.sofkar.questions.webui.formulariovehiculo.CampoObligatorioVacio.campoObligatorioVacio;
import static co.com.sofka.sofkar.questions.webui.formulariovehiculo.CamposIncompletos.camposIncompletos;
import static co.com.sofka.sofkar.questions.webui.formulariovehiculo.VehiculoAgregadoExitosamente.vehiculoAgregadoExitosamente;
import static co.com.sofka.sofkar.tasks.webui.OpenLandingPage.openLandingPage;
import static co.com.sofka.sofkar.tasks.webui.formulariovehiculo.FormularioCompletoCorrecto.formularioCompletoCorrecto;
import static co.com.sofka.sofkar.tasks.webui.formulariovehiculo.FormularioVacio.formularioVacio;
import static co.com.sofka.sofkar.tasks.webui.formulariovehiculo.GoToNewVehicleForm.goToNewVehicleForm;
import static co.com.sofka.sofkar.tasks.webui.login.RegistroCredencialesCorrectas.registroCredencialesCorrectas;
import static co.com.sofka.sofkar.util.CreateModels.loginModelData;
import static co.com.sofka.sofkar.util.CreateModels.newCarModelData;
import static co.com.sofka.sofkar.util.Dictionary.*;
import static co.com.sofka.sofkar.util.Dictionary.PASS_LOGIN;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.CoreMatchers.equalTo;

public class AgregarVehiculoStepDefinition extends SetUp {
    private static final Logger LOGGER = Logger.getLogger(AgregarVehiculoStepDefinition.class);
    private NewCarModel newCarModel = newCarModelData(SPANISH_CODE_LANGUAGE, COUNTRY_CODE);
    private LoginModel loginModel;
    private static final String ACTOR_NAME = "Tester";

    @Dado("que el usuario inicia sesion y navega al formulario de agregar vehiculo")
    public void queElUsuarioIniciaSesionYNavegaAlFormularioDeAgregarVehiculo() {
        try{
            loginModel = loginModelData(EMAIL_LOGIN, PASS_LOGIN);

            actorSetupTheBrowser(ACTOR_NAME);
            theActorInTheSpotlight().wasAbleTo(
                    openLandingPage()
            );
            theActorInTheSpotlight().attemptsTo(
                    registroCredencialesCorrectas()
                            .usingModel(loginModel)
            );
            theActorInTheSpotlight().attemptsTo(
                    goToNewVehicleForm()
            );
        }catch (Exception exception){
            LOGGER.error(exception);
        }
    }

    @Cuando("el usuario llena todos los campos correctamente y envia el formulario")
    public void elUsuarioLlenaTodosLosCamposCorrectamenteYEnviaElFormulario() {
        theActorInTheSpotlight().attemptsTo(
                formularioCompletoCorrecto()
                        .usingModel(newCarModel)
        );
    }

    @Entonces("el sistema mostrara un aviso de vehiculo registrado")
    public void elSistemaMostraraUnAvisoDeVehiculoRegistrado() {
        theActorInTheSpotlight().should(
                seeThat(vehiculoAgregadoExitosamente(), equalTo(true))
        );
    }

    @Cuando("el usuario envia el formulario vacio")
    public void elUsuarioEnviaElFormularioVacio() {
        theActorInTheSpotlight().attemptsTo(
                formularioVacio()
        );
    }

    @Entonces("el sistema mostrara un aviso de campos incompletos")
    public void elSistemaMostraraUnAvisoDeCamposIncompletos() {
        theActorInTheSpotlight().should(
                seeThat(camposIncompletos(), equalTo(true))
        );
    }
}
