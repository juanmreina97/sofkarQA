package co.com.sofka.sofkar.runners.webui;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        strict = true,
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        features = {"src/test/resources/features/webui/login.feature"},
        glue = {"co.com.sofka.sofkar.stepdefinition.webui.login"}
)

public class LoginTest {
}
