package co.com.sofka.sofkar.runners.webui;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        strict = true,
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        features = {"src/test/resources/features/webui/registro.feature"},
        glue = {"co.com.sofka.sofkar.stepdefinition.webui.registro"}
)
public class RegistroTest {
}
