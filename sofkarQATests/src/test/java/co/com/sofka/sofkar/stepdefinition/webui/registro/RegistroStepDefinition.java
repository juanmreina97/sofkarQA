package co.com.sofka.sofkar.stepdefinition.webui.registro;

import co.com.sofka.sofkar.models.RegisterModel;
import co.com.sofka.sofkar.stepdefinition.setup.SetUp;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import org.apache.log4j.Logger;

import static co.com.sofka.sofkar.questions.webui.registro.CamposObligatoriosVacios.camposObligatoriosVacios;
import static co.com.sofka.sofkar.questions.webui.registro.InformacionIncompleta.informacionIncompleta;
import static co.com.sofka.sofkar.questions.webui.registro.PasswordCorta.passwordCorta;
import static co.com.sofka.sofkar.questions.webui.registro.PasswordMinuscula.passwordMinuscula;
import static co.com.sofka.sofkar.questions.webui.registro.PasswordNoCoincide.passwordNoCoincide;
import static co.com.sofka.sofkar.questions.webui.registro.RegistroExitoso.registroExitoso;
import static co.com.sofka.sofkar.tasks.webui.OpenLandingPage.openLandingPage;
import static co.com.sofka.sofkar.tasks.webui.registro.GoToRegisterPage.goToRegisterPage;
import static co.com.sofka.sofkar.tasks.webui.registro.RegistroCorrectoCamposObligatorios.registroCorrectoCamposObligatorios;
import static co.com.sofka.sofkar.tasks.webui.registro.RegistroPasswordCorta.registroPasswordCorta;
import static co.com.sofka.sofkar.tasks.webui.registro.RegistroPasswordMinuscula.registroPasswordMinuscula;
import static co.com.sofka.sofkar.tasks.webui.registro.RegistroPasswordNoCoincide.registroPasswordNoCoincide;
import static co.com.sofka.sofkar.tasks.webui.registro.RegistroVacio.registroVacio;
import static co.com.sofka.sofkar.util.CreateModels.registerModelData;
import static co.com.sofka.sofkar.util.Dictionary.*;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.CoreMatchers.equalTo;

public class RegistroStepDefinition extends SetUp {
    private static final Logger LOGGER = Logger.getLogger(RegistroStepDefinition.class);
    private RegisterModel registerModel = registerModelData(SPANISH_CODE_LANGUAGE, COUNTRY_CODE, EMAIL_DOMAIN);
    private static final String ACTOR_NAME = "Tester";

    @Dado("que el usuario se encuentra en la pagina de registro")
    public void queElUsuarioSeEncuentraEnLaPaginaDeRegistro() {
        try{
            actorSetupTheBrowser(ACTOR_NAME);
            theActorInTheSpotlight().wasAbleTo(
                    openLandingPage()
            );
            theActorInTheSpotlight().attemptsTo(
                    goToRegisterPage()
            );
        }catch (Exception exception){
            LOGGER.error(exception);
        }
    }

    @Cuando("el usuario llena correctamente los campos obligatorios y confirma")
    public void elUsuarioLlenaCorrectamenteLosCamposObligatoriosYConfirma() {
        theActorInTheSpotlight().attemptsTo(
                registroCorrectoCamposObligatorios()
                        .usingModel(registerModel)
        );
    }

    @Entonces("la plataforma mostrara un mensaje de registro exitoso")
    public void laPlataformaMostraraUnMensajeDeRegistroExitoso() {
        theActorInTheSpotlight().should(
                seeThat(registroExitoso(), equalTo(true))
        );
    }

    @Cuando("el usuario ingresa un password con menos de ocho digitos")
    public void elUsuarioIngresaUnPasswordConMenosDeOchoDigitos() {
        theActorInTheSpotlight().attemptsTo(
                registroPasswordCorta()
                        .usingModel(registerModel)
        );
    }

    @Entonces("el sistema mostrara un aviso de error por longitud de password")
    public void elSistemaMostraraUnAvisoDeErrorPorLongitudDePassword() {
        theActorInTheSpotlight().should(
                seeThat(passwordCorta(), equalTo(true))
        );
    }

    @Cuando("el usuario ingresa un password de ocho digitos completamente en minuscula")
    public void elUsuarioIngresaUnPasswordDeOchoDigitosCompletamenteEnMinuscula() {
        theActorInTheSpotlight().attemptsTo(
                registroPasswordMinuscula()
                        .usingModel(registerModel)
        );
    }

    @Entonces("el sistema mostrara un aviso de error por falta de combinacion de caracteres")
    public void elSistemaMostraraUnAvisoDeErrorPorFaltaDeCombinacionDeCaracteres() {
        theActorInTheSpotlight().should(
                seeThat(passwordMinuscula(), equalTo(true))
        );
    }

    @Cuando("los password ingresados por el usuario no coinciden")
    public void losPasswordIngresadosPorElUsuarioNoCoinciden() {
        theActorInTheSpotlight().attemptsTo(
                registroPasswordNoCoincide()
                        .usingModel(registerModel)
        );
    }

    @Entonces("el sistema mostrara un aviso por password diferentes")
    public void elSistemaMostraraUnAvisoPorPasswordDiferentes() {
        theActorInTheSpotlight().should(
                seeThat(passwordNoCoincide(), equalTo(true))
        );
    }

    @Cuando("el usuario se registra con campos obligatorios vacios")
    public void elUsuarioSeRegistraConCamposObligatoriosVacios() {
        theActorInTheSpotlight().attemptsTo(
                registroVacio()
        );
    }

    @Entonces("el sistema mostrara un error de campo obligatorio vacio")
    public void elSistemaMostraraUnErrorDeCampoObligatorioVacio() {
        theActorInTheSpotlight().should(
                seeThat(camposObligatoriosVacios(), equalTo(true)),
                seeThat(informacionIncompleta(), equalTo(true))
        );
    }
}
