package co.com.sofka.sofkar.stepdefinition.webui.login;

import co.com.sofka.sofkar.models.LoginModel;
import co.com.sofka.sofkar.models.RegisterModel;
import co.com.sofka.sofkar.stepdefinition.setup.SetUp;
import co.com.sofka.sofkar.stepdefinition.webui.registro.RegistroStepDefinition;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import org.apache.log4j.Logger;

import static co.com.sofka.sofkar.questions.webui.login.LoginEmailError.loginEmailError;
import static co.com.sofka.sofkar.questions.webui.login.LoginExitoso.loginExitoso;
import static co.com.sofka.sofkar.questions.webui.login.LoginPassError.loginPassError;
import static co.com.sofka.sofkar.questions.webui.registro.RegistroExitoso.registroExitoso;
import static co.com.sofka.sofkar.tasks.webui.OpenLandingPage.openLandingPage;
import static co.com.sofka.sofkar.tasks.webui.login.RegistroCredencialesCorrectas.registroCredencialesCorrectas;
import static co.com.sofka.sofkar.tasks.webui.login.RegistroEmailErroneo.registroEmailErroneo;
import static co.com.sofka.sofkar.tasks.webui.login.RegistroPassErronea.registroPassErronea;
import static co.com.sofka.sofkar.tasks.webui.registro.GoToRegisterPage.goToRegisterPage;
import static co.com.sofka.sofkar.util.CreateModels.loginModelData;
import static co.com.sofka.sofkar.util.CreateModels.registerModelData;
import static co.com.sofka.sofkar.util.Dictionary.*;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.CoreMatchers.equalTo;

public class LoginStepDefinition extends SetUp {

    private static final Logger LOGGER = Logger.getLogger(LoginStepDefinition.class);
    private LoginModel loginModel = loginModelData(EMAIL_LOGIN, PASS_LOGIN);
    private static final String ACTOR_NAME = "Tester";


    @Dado("que el usuario se encuentra en la pagina de ingreso")
    public void queElUsuarioSeEncuentraEnLaPaginaDeIngreso() {
        try{
            actorSetupTheBrowser(ACTOR_NAME);
            theActorInTheSpotlight().wasAbleTo(
                    openLandingPage()
            );

        }catch (Exception exception){
            LOGGER.error(exception);
        }

    }

    @Cuando("el usuario llena correctamente las credenciales")
    public void elUsuarioLlenaCorrectamenteLasCredenciales() {
        theActorInTheSpotlight().attemptsTo(
                registroCredencialesCorrectas()
                        .usingModel(loginModel)

        );

    }
    @Entonces("la plataforma mostrara la opcion de cerrar sesion")
    public void laPlataformaMostraraLaOpcionDeCerrarSesion() {
        theActorInTheSpotlight().should(
                seeThat(loginExitoso(), equalTo(true))
        );


    }

    @Cuando("el usuario ingresa un correo no registrado")
    public void elUsuarioIngresaUnCorreoNoRegistrado() {

        theActorInTheSpotlight().attemptsTo(
                registroEmailErroneo()
                        .usingModel(loginModel)

        );

    }
    @Entonces("el sistema mostrara un mensaje de error")
    public void elSistemaMostraraUnMensajeDeError() {
        theActorInTheSpotlight().should(
                seeThat(loginEmailError(), equalTo(true))
        );

    }

    @Cuando("el usuario ingresa un password incorrecto")
    public void elUsuarioIngresaUnPasswordIncorrecto() {

        theActorInTheSpotlight().attemptsTo(
                registroPassErronea()
                        .usingModel(loginModel)

        );

    }

    @Entonces("el sistema lanzara mensaje de error")
    public void elSistemaLanzaraMensajeDeError() {

        theActorInTheSpotlight().should(
                seeThat(loginPassError(), equalTo(true))
        );

    }

}
